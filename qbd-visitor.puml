@startuml
interface IVisitor<TVisited> {
    Visit(TVisited)
}

'Serialization Entities
package "Quick books XML response" {


abstract class QuickBooksResponse {
    + StatusCode
    + StatusMessage
    + StatusSeverity
}
abstract class InvoiceImportResponse extends QuickBooksResponse {
    Validate(QuickBooksInvoiceResponseValidator, Invoice)
}
class BillQueryResponse extends InvoiceImportResponse {
    + BillQueryDetail
}
class BillDebitAddResponse extends InvoiceImportResponse {
    + BillDebitDetail
}
class BillCreditAddResponse extends InvoiceImportResponse {
    + BillCreditDetail
}
}
package "Serialized Bill" {
abstract class BillDetail {
    + APAccountRef
    + CurrencyRef
    + VendorRef
    + EditSequence
    + ExchangeRate
    + ItemLineRet[]
    + ExpenseLineRet[]
    + ExternalGUID
    + IsTaxIncluded
    + Memo
    + OpenAmount
    + RefNumber
    + TimeCreated
    + TimeModified
    + TxnDate
    + TxnID
    + TxnNumber

    Validate(BillDetailValidator, Invoice)
}
class BillQueryDetail extends BillDetail {
}
class BillDebitDetail extends BillDetail {
    + AmountDue
    + AmountDueHomeCurrency
    + DueDate
    + IsPaid
    + LinkToTxn
    + LinkToTxnID
    + TermsRef
    + VendorAddress
}
class BillCreditDetail extends BillDetail {
    + CreditAmount
    + CreditAmountInHomeCurrency
}
}
'Visitors
abstract class QuickBooksResponseVisitor<QuickBooksResponse> extends IVisitor {
    {abstract} Visit(QuickBooksResponse)
}
class QuickBooksInvoiceResponseValidator extends QuickBooksResponseVisitor {
    + Invoice Invoice
    + Visit(QuickBooksResponse) { validate status code }
}

abstract class BillDetailVisitor<BillDetail> extends IVisitor {
    + Invoice Invoice
    {abstract} Visit(BillDetail)
}
package "Bill Detail Validation" {

    class BillDetailValidator extends BillDetailVisitor {
        + Visit(BillDetail)
    }
    class BillQueryDetailValidator extends BillDetailValidator {
        + Visit(BillDetail) { base.Visit(BillDetail) }
    }
    class BillDebitDetailValidator extends BillDetailValidator {
        + Visit(BillDetail) { base.Visit(BillDetail) ... }
    }
    class BillCreditDetailValidator extends BillDetailValidator {
        + Visit(BillDetail) { base.Visit(BillDetail) ... }
    }
}

BillDetail *-- BillDetailVisitor
QuickBooksResponse *-- QuickBooksResponseVisitor

@enduml
